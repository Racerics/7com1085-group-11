Search String
*************

(((cloud OR {big data} OR {virtual network}) AND (comput* OR system* OR storage*) )wn TI 
	AND ((change* OR modifi* ) AND (security OR trust OR reliability OR privacy OR firewall* OR intrusion*) 
	AND (users OR clients OR customers) AND (cost OR rent OR price))wn AB)

Inclusion criteria
*******************

- Most cited papers
- research done by empirical studies
- published after 2013
- written in english


Exclusion criteria
*******************

- publications before 2013
- publications that did not conduct an empricial study
- publications not written in English
- duplicates of previous work


Digital libraries
******************

- IEEE Xplore
- ScienceDirect
- ACM Journals
- Springer


